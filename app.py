import os
import requests
from flask import Flask, render_template, request
import pymysql
import json

app = Flask(__name__)

try:
    conn = pymysql.connect(
        user='ccdbadmin',
        passwd=os.environ.get('DB_KEY'),
        host='cc-db-maunal.mysql.database.azure.com',
        database='cc-main',
        ssl={'ca': 'DigiCertGlobalRootCA.crt.pem'}
    )

    print("Connection established")
except Exception as e:
    print("Something went wrong while connecting to the db, exiting")
    exit()

cursor = conn.cursor()

def create_table():
    cursor.execute("SELECT COUNT(*) FROM information_schema.tables WHERE table_name = 'questions';")
    if cursor.fetchone()[0] == 1:
        print("table already existed")
    else:
        cursor.execute("CREATE TABLE questions (question VARCHAR(500) PRIMARY KEY, answer TEXT(50000));")
        print("created new table")

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/answer', methods=['POST'])
def answer():
    received_question = request.form['text-input']
    
    answer = "this should be replaced"
    
    question = received_question.lower()
    
    sql = "SELECT answer FROM questions WHERE question= %s"
    param = (question,)
    
    # definitely an injection possible
    cursor.execute(sql, param)
    row = cursor.fetchone()
    if row is not None:
        answer = row[0] + " --- (fetched from db)"
    else:
        # TODO: qna plugin: os.environ.get('QNA_KEY')
        
        #with qna_client:
        #    answer = qna_client.get_answers(
        #        question=question,
        #        project_name="",
        #        deployment_name="production"   
        #    )
            
        #with ta_client:
        #    answer = qna_client.get_answers(
        #        question=question,
        #        project_name="cc-kb",
        #        deployment_name="production"   
        #    )
        
        url = "https://cc-qna-manual.cognitiveservices.azure.com/language/:query-knowledgebases?projectName=cc-kb&api-version=2021-10-01&deploymentName=production"
 
        headers = {
            "Ocp-Apim-Subscription-Key": os.environ.get('QNA_KEY'),
            "Content-Type": "application/json"
        }
         
        data = { 
            "top": 3,
            "question": question,
            "includeUnstructuredSources": True,
            "confidenceScoreThreshold": "0.5",
            "answerSpanRequest": {
                "enable": True,
                "topAnswersWithSpan": 1,
                "confidenceScoreThreshold": "0.5"
            }
        }
        
        response = requests.post(url, headers=headers, json=data)
        if response.status_code == 200:
            answer = response.json()['answers'][0]['answer']
        else:
            answer = "An error occured while fetching data from the cognitive service"
        
        sql = "INSERT INTO questions (question, answer) VALUES (%s, %s)"
        param = (question, answer,)
        
        cursor.execute(sql, param)
        
        conn.commit()
        
        print("Added a new entry to the table")

    return render_template('index.html', user_question=received_question, output=answer)

if __name__ == '__main__':
    create_table()
    app.run(host='0.0.0.0', port=5000)
